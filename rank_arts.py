import urllib.parse
import requests
import telebot
import configparser
import httplib2
import pandas as pd
from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials


config = configparser.ConfigParser()
config.read("./config")
TOKEN = config['telegram']['TOKEN']
bot = telebot.TeleBot(TOKEN)
channel_id = config['telegram']['channel_id']

sheet = config['sheet']['danya']

scopes = ['https://www.googleapis.com/auth/spreadsheets']
creds_service = ServiceAccountCredentials.from_json_keyfile_name\
    ('./credits.json', scopes).authorize(httplib2.Http())
my_table = build('sheets', 'v4', http=creds_service)
table = my_table.spreadsheets().values().batchGet(spreadsheetId=sheet, ranges="Ранг карточек").execute()
df_table = pd.DataFrame(table['valueRanges'][0]['values'])
df_table.columns = df_table.loc[0, :]
df_table.drop(labels=0, axis=0, inplace=True)


def requests_art(art, ident, word):
    # print(art, ident, word)
    url_word = urllib.parse.quote(word.encode('utf-8'))
    url = 'https://www.wildberries.ru/catalog/0/search.aspx?search={url_word}'.format(url_word=url_word)
    headers = {
        'Accept': '*/*',
        'Accept-Language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
        'Connection': 'keep-alive',
        'Origin': 'https://www.wildberries.ru',
        'Referer': url,
        'Sec-Fetch-Dest': 'empty',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Site': 'cross-site',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36',
        'sec-ch-ua': '"Chromium";v="118", "Google Chrome";v="118", "Not=A?Brand";v="99"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"Windows"',
        'x-userid': '6536278',
    }

    response = requests.get(
        'https://search.wb.ru/exactmatch/ru/common/v4/search?TestGroup=ty_rec_rem_cat_compl&TestID=336&appType=1&curr=rub&dest=-1252465&query={url_word}'
        '&regions=80,83,38,4,64,33,68,70,30,40,86,75,69,1,66,110,22,48,31,71,112,114&resultset=catalog&sort=popular&spp=29&suppressSpellcheck=false&uclusters=0'.
        format(url_word=url_word),
        headers=headers,
    )
    for i in response.json()['data']['products']:
        if str(i['id']) == str(art):
            # print(response.json()['data']['products'].index(i))
            if response.json()['data']['products'].index(i) >= 2:
                position = response.json()['data']['products'].index(i) + 1
                main_url = "https://www.wildberries.ru/catalog/{ART}/detail.aspx".format(ART=art)
                string_warn = '[{product}]({url}) '.format(product=ident, url=main_url) + \
                              'на {position} месте в поиске'.format(position=position) + ' по слову "{test}"'.format(test=word)
                bot.send_message(chat_id=channel_id, text=string_warn, parse_mode='Markdown')
            return response.json()['data']['products'].index(i)+1


for word in df_table['Запрос'].tolist():
    art = df_table.loc[(df_table['Запрос'] == word), ['Артикул']].values[0][0]
    ident = df_table.loc[(df_table['Запрос'] == word), ['Идентификатор']].values[0][0]
    # print(art, ident, word)
    rank = requests_art(art, ident, word)
    df_table.loc[(df_table['Запрос'] == word) & (df_table['Артикул'] == art), ['Место в выдаче']] = rank

    body = {
        'data': [
            {
                'range': 'Ранг карточек',
                'majorDimension': 'COLUMNS',
                'values': [df_table.columns.values.tolist()]
            },
            {
                'range': 'Ранг карточек',
                'majorDimension': 'ROWS',
                'values': df_table.values.tolist()
            }
        ]
    }

    request = my_table.spreadsheets().values().update(spreadsheetId=sheet,
                                                      valueInputOption='USER_ENTERED',
                                                      range='Ранг карточек',
                                                      body={'values': [
                                                                          df_table.columns.tolist()] + df_table.values.tolist()}).execute()

    # print(rank)

